import React, { useState,useEffect } from "react";
import { FlatList, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity,View } from "react-native";
import * as Contacts from 'expo-contacts'
import Btn from "../components/Btn"

import firebase from 'firebase/app';
import "firebase/auth";





const Item = ({ item, onPress, backgroundColor, textColor }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
    <Text style={[styles.title, textColor]}>{item.title}</Text>
  </TouchableOpacity>
);

const App = () => {

  const [selectedId, setSelectedId] = useState(null);
  const [contacts,setContacts] = useState()
  useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contacts.getContactsAsync({
          fields: [Contacts.Fields.FirstName],
        });

        if (data.length > 0) {
          setContacts(data)
          console.log(data)
        }
      }
    })();
  }, []);

 

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={contacts}
        keyExtractor={(item) => item.id}
        renderItem={({item}) => {
            const backgroundColor = item.id === selectedId ? "#6e3b6e" : "#f9c2ff";
            const color = item.id === selectedId ? 'white' : 'black';

            return(
                <View>
                <Text>{item.name}</Text>
                
                </View>

            )
        }}
        
        extraData={selectedId}
      />

      <View style={styles.view}>
        <Text style={{fontSize: 34, fontWeight: "800", marginBottom: 20}}>bruh</Text>
        <Btn title="Log Out" onClick={() => firebase.auth().signOut()} />
    </View>
    </SafeAreaView>
    
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default App;